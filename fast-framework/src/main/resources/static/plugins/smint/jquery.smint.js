/*
下拉滚动,悬浮
使用:
$('#__search').smint({
	'scrollSpeed' : 1000
});
SMINT V1.0 by Robert McCracken

SMINT is my first dabble into jQuery plugins!

http://www.outyear.co.uk/smint/

If you like Smint, or have suggestions on how it could be improved, send me a tweet @rabmyself

*/
(function(){

	
	$.fn.smint = function( options ) {

		// adding a class to users div
		$(this).addClass('smint')

		var settings = $.extend({
            'scrollSpeed '  : 500,
			'spanTxt' : ''
            }, options);


		return $('.smint a').each( function() {

            
			if ( settings.scrollSpeed ) {

				var scrollSpeed = settings.scrollSpeed

			}


			///////////////////////////////////

			// get initial top offset for the menu 
			var stickyTop = $('.smint').position().top;
			console.log('--->>',stickyTop);
			// check position and make sticky if needed
			var stickyMenu = function(){
				
				// current distance top
				var scrollTop = $(window).scrollTop(); 
							
				// if we scroll more than the navigation, change its position to fixed and add class 'fxd', otherwise change it back to absolute and remove the class
				if (scrollTop > stickyTop) {
					if(settings.spanTxt !== ''){
						$('.smint').css({ 'position': 'fixed', 'top':0, 'z-index': 19920219 }).addClass('fxd');
						if($('.smint').find('._smintspan')){
							$('.smint').find('._smintspan').remove();
						}
						$('.smint').append('<div class="_smintspan" style="color: rgb(255, 255, 255);\n' +
							'    width: 40px;\n' +
							'    font-size: 12px;\n' +
							'    padding: 2px 0px 3px;\n' +
							'    background-color: rgb(227, 42, 22);\n' +
							'    line-height: inherit;\n' +
							'    text-align: center;\n' +
							'    position: absolute;\n' +
							'    top: 5px;\n' +
							'    right: 15px;\n' +
							'    transform-origin: center center;\n' +
							'    transform: rotate(40deg) scale(0.8);\n' +
							'    opacity: 0.95;\n' +
							'    z-index: 2;\n' +
							'    border-radius: 50%;">'+settings.spanTxt+'</div>');
					}else{
						$('.smint').css({ 'position': 'fixed', 'top':0, 'z-index': 19920219 }).addClass('fxd');
					}
				} else {
					$('.smint').css({ 'position': 'relative', 'top': stickyTop }).removeClass('fxd').find('._smintspan').remove();
				}
			};
					
			// run function
			// stickyMenu();
					
			// run function every time you scroll
			$(window).scroll(function() {
				 stickyMenu();
			});

			///////////////////////////////////////
    
        
        	// $(this).on('click', function(e){
			//
			// 	// gets the height of the users div. This is used for off-setting the scroll so th emenu doesnt overlap any content in the div they jst scrolled to
			// 	var selectorHeight = $('.smint').height();
			//
        	// 	// stops empty hrefs making the page jump when clicked
			// 	e.preventDefault();
			//
			// 	// get id pf the button you just clicked
		 	// 	var id = $(this).attr('id');
			//
			// 	// gets the distance from top of the div class that matches your button id minus the height of the nav menu. This means the nav wont initially overlap the content.
			// 	var goTo =  $('div.'+ id).offset().top -selectorHeight;
			//
			// 	// Scroll the page to the desired position!
			// 	$("html, body").animate({ scrollTop: goTo }, scrollSpeed);
			//
			// });

            

		});

	}

})();
